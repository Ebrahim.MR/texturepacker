﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ETexturePacker {
    public class Core {
        public struct PackingData {
            public string RMapPath;
            public string GMapPath;
            public string BMapPath;
            public string AMapPath;
            public string OutputPath;
            public bool IsPNG;

            public bool UseRLE;
            public bool UseNewFormat;
            public bool ColorMap2BytesEntry;

            public void Reset() {
                RMapPath = string.Empty;
                GMapPath = string.Empty;
                BMapPath = string.Empty;
                AMapPath = string.Empty;
                OutputPath = string.Empty;
                IsPNG = true;
                UseRLE = false;
                UseNewFormat = true;
                ColorMap2BytesEntry = false;
            }
        }
        public static Bitmap Pack(PackingData packingData, ref BackgroundWorker worker) {
            //return (int)(specImage.GetPixel(50, 50).GetBrightness() * 255);
            bool IsRMapInvalid = false;
            bool IsGMapInvalid = false;
            bool IsBMapInvalid = false;
            bool IsAMapInvalid = false;

            int width = 0;
            int height = 0;

            int progress = 0;
            float xResolution = 0, yResolution = 0;

            Bitmap RMap;
            if (File.Exists(packingData.RMapPath)) {
                RMap = ToGrayscale(Image.FromFile(packingData.RMapPath) as Bitmap);
                width = RMap.Width;
                height = RMap.Height;
                xResolution = RMap.HorizontalResolution;
                yResolution = RMap.VerticalResolution;

                worker.ReportProgress(progress += 5);
            } else {
                IsRMapInvalid = true;
                RMap = new Bitmap(1, 1);
            }

            Bitmap GMap;
            if (File.Exists(packingData.GMapPath)) {
                GMap = ToGrayscale(Image.FromFile(packingData.GMapPath) as Bitmap);
                width = GMap.Width;
                height = GMap.Height;
                xResolution = GMap.HorizontalResolution;
                yResolution = GMap.VerticalResolution;

                worker.ReportProgress(progress += 5);
            } else {
                IsGMapInvalid = true;
                GMap = new Bitmap(1, 1);
            }

            Bitmap BMap;
            if (File.Exists(packingData.BMapPath)) {
                BMap = ToGrayscale(Image.FromFile(packingData.BMapPath) as Bitmap);
                width = BMap.Width;
                height = BMap.Height;
                xResolution = BMap.HorizontalResolution;
                yResolution = BMap.VerticalResolution;

                worker.ReportProgress(progress += 5);
            } else {
                IsBMapInvalid = true;
                BMap = new Bitmap(1, 1);
            }

            Bitmap AMap;
            if (File.Exists(packingData.AMapPath)) {
                AMap = ToGrayscale(Image.FromFile(packingData.AMapPath) as Bitmap);
                width = AMap.Width;
                height = AMap.Height;
                xResolution = AMap.HorizontalResolution;
                yResolution = AMap.VerticalResolution;

                worker.ReportProgress(progress += 5);
            } else {
                IsAMapInvalid = true;
                AMap = new Bitmap(1, 1);
            }

            if (width == 0) {
                throw new System.Exception("NO TEXTURE IS SELECTED. PLEASE SELECT TEXTURES.");
            }

            if (IsRMapInvalid) {
                RMap.Dispose();
                RMap = new Bitmap(width, height);
                RMap.SetResolution(xResolution, yResolution);
                worker.ReportProgress(progress += 5);
            }

            if (IsGMapInvalid) {
                GMap.Dispose();
                GMap = new Bitmap(width, height);
                GMap.SetResolution(xResolution, yResolution);
                worker.ReportProgress(progress += 5);
            }

            if (IsBMapInvalid) {
                BMap.Dispose();
                BMap = new Bitmap(width, height);
                BMap.SetResolution(xResolution, yResolution);
                worker.ReportProgress(progress += 5);
            }

            if (IsAMapInvalid) {
                AMap.Dispose();
                AMap = new Bitmap(width, height);
                AMap.SetResolution(xResolution, yResolution);
                worker.ReportProgress(progress += 5);
            }

            Bitmap outputImage = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            outputImage.SetResolution(xResolution, yResolution);

            int i, j, R, G, B, A;
            try {
                for (i = 0; i < height; i++) {
                    for (j = 0; j < width; j++) {

                        R = (int)(RMap.GetPixel(j, i).GetBrightness() * 255);
                        G = (int)(GMap.GetPixel(j, i).GetBrightness() * 255);
                        B = (int)(BMap.GetPixel(j, i).GetBrightness() * 255);
                        A = (int)(AMap.GetPixel(j, i).GetBrightness() * 255);

                        Color pixelColor = Color.FromArgb(A, R, G, B);

                        outputImage.SetPixel(j, i, pixelColor);
                    }
                    
                    worker.ReportProgress((int)(20 + 70f * ((i + 1) / (float)height)));
                }
            } catch (Exception ex) {
                if (ex.Message.Contains("Width") || ex.Message.Contains("Height")) {
                    throw new Exception("ERROR: TEXTURES SIZE ARE NOT EQUAL.");
                } else {
                    throw ex;
                }
            }

            if (packingData.IsPNG) {
                outputImage.Save(packingData.OutputPath, ImageFormat.Png);
            } else {
                var TGA = new TGASharpLib.TGA(outputImage, packingData.UseRLE, packingData.UseNewFormat, packingData.ColorMap2BytesEntry);
                TGA.Save(packingData.OutputPath);
            }

            worker.ReportProgress(100);
            return outputImage;
        }
        public static Bitmap ToGrayscale(Bitmap original) {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            using (Graphics g = Graphics.FromImage(newBitmap)) {

                //create the grayscale ColorMatrix
                ColorMatrix colorMatrix = new ColorMatrix(
                   new float[][]
                   {
             new float[] {.3f, .3f, .3f, 0, 0},
             new float[] {.59f, .59f, .59f, 0, 0},
             new float[] {.11f, .11f, .11f, 0, 0},
             new float[] {0, 0, 0, 1, 0},
             new float[] {0, 0, 0, 0, 1}
                   });

                //create some image attributes
                using (ImageAttributes attributes = new ImageAttributes()) {

                    //set the color matrix attribute
                    attributes.SetColorMatrix(colorMatrix);

                    //draw the original image on the new image
                    //using the grayscale color matrix
                    g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
                                0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);
                }
            }
            return newBitmap;
        }
    }
}